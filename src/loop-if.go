package main

import (
  "fmt"
)


func main() {

    total := 0
    angka := 150
    colour := "Ungu"

    for NUM:=1; NUM <=10; NUM++ {
    total = total + NUM

    fmt.Println("Looping ", NUM)
    fmt.Println("Tambah deret baris ", total)
    fmt.Println("Total deret baris ", total + NUM)
  }

   if angka < 100 {
      fmt.Println("Lebih kecil dari 100")
   } else {
      fmt.Println("Lebih besar dari 100")
   }

   if colour == "Ungu" {
      fmt.Println("Hasil nya warna ", colour)
   } else {
      fmt.Println("Hasilnya bukan warna ", colour)
   }

}
