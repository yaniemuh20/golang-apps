package main

import (
  "fmt"
  "strconv"
)

func multiply(angka1 int, angka2 int) int {
	return angka1 * angka2
}

func getBio(age int, name string, status string) string {
	ageNow := strconv.Itoa(age)
	return name + " adalah seorang " + status + " saat ini berumur " + ageNow + " tahun"
}

func getBio2(age int, name string, status string) (string,string) {
        ageNow := strconv.Itoa(age)
        return name + " adalah seorang " + status,
	       "Umurnya Skrng " + ageNow
}

func getBio3(age int, name string, status string) (bio string, ageIn int) {
        bio = name + " adalah seorang " + status
	ageIn = age + 10

	return
}



func main() {
//basicInfo, ageInfo := getBio2(30 , "Denniz", "Pemain Voli")
basicInfo, ageInfo := getBio3(30 , "Denniz", "Pemain Voli")

    fmt.Println("fungsi ini menghasilkan", multiply(10, 5))
    fmt.Println("fungsi ini menghasilkan", multiply(3, 8))
    fmt.Println(getBio(25 , "Dybala", "Pemain bola"))
    fmt.Println(getBio(23 , "Norah", "Pemain Basket"))
    fmt.Println(basicInfo, ageInfo)
    fmt.Println("Umurnya 10 Tahun mendatang", ageInfo)
}
